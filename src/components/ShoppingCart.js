import React ,{useState} from 'react';
import {FaTrashAlt} from "react-icons/fa";
import Modal from "./Modal";


function ShoppingCart({shopCart , removeFromCart ,  DeleteModal , deleteModal }) {
    const [modalDelete , setmodalDelete] = useState(false)


    const handleModalDelete = () => {
        setmodalDelete(!modalDelete)

    }
    if (!shopCart || shopCart.length === 0) {
        return <p>Your cart is empty.</p>;
    }

    return (
        <>
        <div className="card-wrapper">
            {
                shopCart.map((cartItem)=> (
                    <div key={cartItem.id} className="card-all" >
                        <div className="img-wrapper">
                        <img src={cartItem.image} alt="logo"/>
                        </div>
                        <div className="text-wrapper">
                        <h2>{cartItem.name}</h2>
                        <p>Article: {cartItem.article}</p>
                        <p>Price: {cartItem.price} $</p>
                        <p>Color {cartItem.color}</p>
                        </div>
                        <FaTrashAlt className="btn-trash" onClick={ ()=>{
                            handleModalDelete();
                            DeleteModal(cartItem)
                        }} />
                    </div>)
                )
            }
        </div>
        <div>
            {
            modalDelete && <Modal ModalType={"DELETE"} handleModalDelete={handleModalDelete} removeFromCart={removeFromCart} title={"Delete ?"} text={"You want delete this Item ?"} deleteModal={deleteModal}/>
            }
        </div>
        </>
    );
}

export default ShoppingCart;