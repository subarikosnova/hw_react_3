import React from 'react';
import {FaRegStar} from "react-icons/fa";

function Favorites({favor , DeleteStar}) {
    if (!favor || favor.length === 0) {
        return <p>Your fav is empty.</p>;
    }

    return (
        <div className="card-wrapper">
            {
                favor.map((cartItem)=> (
                    <div key={cartItem.id} className="card-all">
                        <FaRegStar className="star-infav" onClick={() => DeleteStar(cartItem)}/>
                        <div className="img-wrapper">
                        <img src={cartItem.image} alt="logo"/>
                        </div>
                        <div className="text-wrapper">
                        <h2>{cartItem.name}</h2>
                        <p>Article: {cartItem.article}</p>
                        <p>Price: {cartItem.price} $</p>
                        <p>Color {cartItem.color}</p>
                        </div>
                    </div>)
                )
            }
        </div>
    );
}

export default Favorites;