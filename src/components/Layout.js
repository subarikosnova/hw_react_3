import React, {useState} from 'react';
import DotaLogo from '../img/dotalogo.jpg'
import {FaCartShopping} from "react-icons/fa6";
import {FaRegStar} from "react-icons/fa";
import {Link, Outlet} from 'react-router-dom';
import Footer from "./Footer";


function Layout({ ToggleCart, shopCart, favor}) {
    const [favStar, setFavStar] = useState(false)

    return (
        <>
            <header>
                <Link to="/">
                    <img src={DotaLogo} alt="logo"/>
                </Link>
                <Link to="/">
                    <h1>DOTA ALL STARS SHOP</h1>
                </Link>
                <div className="btn-header">
                   <div className="favorites-wrapper">
                       <Link to="favorites">
                           <FaRegStar className="btn-star" onClick={() => setFavStar(!favStar)}/>
                       </Link>
                       <span >{favor.length}</span>
                    </div>
                    <div className="shopcart-wrapper">
                        <Link to="shopcart">
                            <FaCartShopping className="btn-shop" onClick={ToggleCart}/>
                        </Link>
                        <span>{shopCart.length}</span>
                    </div>
                </div>
            </header>

            <main>
                <Outlet/>
            </main>
            <Footer/>
        </>
    );
}

export default Layout;