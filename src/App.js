import React, {useEffect, useState} from "react";
import Layout from "./components/Layout";
import Style from './App.css'
import Card from "./components/Card";
import Modal from "./components/Modal";
import {Routes, Route,} from "react-router-dom";
import ShoppingCart from "./components/ShoppingCart";
import Favorites from "./components/Favorites";


const App = () => {
    const [data , setData] = useState([])
    const [modal, setModal] = useState (false)
    const [shopCart , setShopCart] = useState([])
    const [buyElement, setBuyElement] = useState ()
    const [cart , setCart] = useState (false)
    const [favor, setFavor] = useState([]);
    const [deleteModal , setDeleteModal] = useState()


    const DeleteModal = (item) => {
        setDeleteModal(item)
    }

    const FavArray = (newStarArray) => {
         setFavor([...favor ,newStarArray])
    }

    const DeleteStar = (itemId) => {
      const updateStar = favor.filter((item) => item.id !== itemId.id)
        setFavor(updateStar)
    }

    const removeFromCart = (itemId) => {
        const updatedCart = shopCart.filter((item) => item.id !== itemId.id);
        setShopCart(updatedCart);
    };



    const ToggleCart = () => {
        setCart(!cart)
    }
    const ElementAdd = (itemCart) => {
        setBuyElement(itemCart)
    }

    const AddToShopCart = (itemCart) => {
        let value = false
        shopCart.forEach((el) => {
            if (el.id === itemCart.id) {
                 value = true
            }
        } )
        !value && setShopCart([...shopCart, itemCart])
    }

    const handleModalOpen = () => {
        setModal(!modal);
    };

    useEffect(() => {
        const storedStar = JSON.parse(localStorage.getItem("star")) || [];
        setFavor(storedStar);
    }, []);

    useEffect(() => {
        localStorage.setItem("star", JSON.stringify(favor));
    }, [favor]);


    useEffect(() => {
        const storedCart = JSON.parse(localStorage.getItem("cart")) || [];
        setShopCart(storedCart);
    }, []);

    useEffect(() => {
        localStorage.setItem("cart", JSON.stringify(shopCart));
    }, [shopCart]);


    useEffect(() => {
        const fetchItems = async () => {
            const response = await fetch(`/product.json`);
            const fetchedData = await response.json();
            setData(fetchedData.item);
        };
        fetchItems();
    }, []);
    return (
        <>
            <Routes>
                <Route path="/" element={<Layout Card={cart} ToggleCart={ToggleCart} shopCart={shopCart} favor={favor} removeFromCart={removeFromCart}  />}>
                    <Route index path="/" element={<Card data={data} handleModalOpen={handleModalOpen} ElementAdd={ElementAdd} FavArray={FavArray} DeleteStar={DeleteStar} />}/>
                    <Route path="favorites" element={<Favorites favor={favor} DeleteStar={DeleteStar}/>}/>
                    <Route path="shopcart" element={<ShoppingCart shopCart={shopCart} removeFromCart={removeFromCart} DeleteModal={DeleteModal} deleteModal={deleteModal}/>}/>
                </Route>
            </Routes>

        <div>
            {
                modal && <Modal ModalType={"BUY"} handleModalOpen={handleModalOpen} AddToShopCart={AddToShopCart} buyElement={buyElement} title={"Want Buy?"} text={"accept click BUY or decline click CLOSE"} />
            }
        </div>
        </>
    );
}

export default App;
